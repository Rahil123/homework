package com.ingress.homework.service;

import com.ingress.homework.dto.StudentDto;

public interface StudentService {

    StudentDto getStudentById(Long id);
    StudentDto addStudent(StudentDto studentDto);
    void deleteStudent(Long id);
}
