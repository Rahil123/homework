package com.ingress.homework.controller;

import com.ingress.homework.dto.StudentDto;
import com.ingress.homework.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;


    @GetMapping(value = "/id/{id}")
    public StudentDto getStudentById(@PathVariable(value = "id") long id) {
        return studentService.getStudentById(id);
    }

    @GetMapping(value = "/")
    public StudentDto getStudent() {
        long id=1L;
        return studentService.getStudentById(id);
    }

}
